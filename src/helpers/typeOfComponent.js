'use strict'

module.exports = (a, b) => {
  var regex = /(.*)-.*/gm
  var result = regex.exec(a)
  if (result) {
    return result[1] === b
  }
  return false
}
